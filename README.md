# 기본 세팅하기

cluster, pods, service(LoadBalancer)를 만든다.

### AWS IAM AUTHENTOCATOR 설치

`brew install aws-iam-authenticator`
`ln -s /usr/local/bin/aws-iam-authenticator /usr/local/bin/heptio-authenticator-aws`

### CLUSTER 만들기

`eksctl create cluster --name=sample --asg-access --nodes-min=1 --nodes-max=5 --node-type=t2.small`


### 노드 확인하기

`kubectl get nodes`

1개 있으면 정상

### sample.yaml 만들기

```
apiVersion: apps/v1 # 보통 v1
kind: Deployment # 리소스 종류
metadata:
  name: web-server
  labels:
    app: web
spec: # 리소스 상세 스펙
  replicas: 3 # replicas 수
  selector:
    matchLabels:
      app: web # 아래의 labels에 app 이름과 매치가 되어야한다.
  template:
    metadata:
      labels:
        app: web
        version: v1
    spec:
      containers:
      - image: wgenesis23/sample:v2
        name: web
        imagePullPolicy: Always
        ports:
        - containerPort: 80
```

### sample.yml 적용하기

`kubectl apply -f sample.yaml`

### pod 상태 확인하기

##### pod list 확인하기

`kubectl get pods -w`

(pod이 replicas 수(여기에서는 3개)이면 정상)


##### pod 자세한 내용보기

`kubectl describe pod web-server-64786c6449-9njrd(pod list의 이름)`

### sample-service.yaml만들기

LoadBalancer를 만듦.
Type이 NodePort면 LoadBalancer 없이 직접 연결도 가능함.

```
apiVersion: v1
kind: Service
metadata:
  name: web-lb
  labels:
    app: web
    version: v1
spec:
  type: LoadBalancer # ClusterIp(default), NodePort, ExternalName
  ports:
  - port: 80
  selector:
    app: web
```

### sample-service.yaml 적용하기

`kubectl apply -f sample-service.yaml`

### 서비스 자세한 상태 확인하기

`kubectl get service`

`kubectl describe service web-lb`

### (뭔가 잘 안된다 싶으면 삭제하고 하면 됨.)

`kubectl delete deployment web-server`

# Blue Green(Deployment 2개) 해보기

### sample-v2.yaml 만들기

```
apiVersion: apps/v1 # 보통 v1
kind: Deployment # 리소스 종류
metadata:
  name: web-server-v2 # 이름을 바꾼다
  labels:
    app: web
spec: # 리소스 상세 스펙
  replicas: 1
  selector:
    matchLabels:
      app: web # labels에 app 이름과 매치가 되어야한다.
  template:
    metadata:
      labels:
        app: web
        version: v2 # 버전도 바꾼다
    spec:
      containers:
      - image: wgenesis23/sample:v2
        name: web
        imagePullPolicy: Always
        ports:
        - containerPort: 80
```

### 배포하기

`kubectl apply -f sample-v2.yaml`

`kubectl get pods`

pods의 NAME에 web-server-v2 추가되면 완료


# HPA - Autoscaling pods

### helm 세팅

##### helm install

```
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > get_helm.sh
chmod +x get_helm.sh
./get_helm.sh
```

##### tiller 설정
tiller - 쿠버네티스 클러스터에서 helm을 사용할 수 있도록 ServiceAccount 에 적용

rbac.yaml 파일 만들기

```
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: tiller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system
```

`kubectl apply -f rbac.yaml`

`helm init --service-account tiller`

##### 틸러 있는지 확인

`kubectl get pods -n kube-system`

tiller 있으면 정상

# CA - Autoscaling pods

### 수동으로 cluster를 수정하려면

클러스터 이름 가지고 오기

`eksctl get cluster`

`eksctl get nodegroups --cluster=sample(클러스터 이름)`

nodes 숫자만큼 ec2를 만듦.

`eksctl scale nodegroup --cluster=sample --nodes=3 ng-d84af329(이름)`


### CA 설정

##### yaml 파일 받기 

`wget https://eksworkshop.com/scaling/deploy_ca.files/cluster_autoscaler.yml`

##### 수정할 부분

```
          command:
            - ./cluster-autoscaler
            - --v=4
            - --stderrthreshold=info
            - --cloud-provider=aws
            - --skip-nodes-with-local-storage=false
            - --nodes=1:5:eksctl-sample-nodegroup-ng-d84af329-NodeGroup-T3TE2AWETDYZ
          env:
            - name: AWS_REGION
              value: ap-northeast-1
```

aws console가서 aws가서 min값, max값 AS이름을 가지고 와야함.

- --nodes=1:5:eksctl-sample-nodegroup-ng-d84af329-NodeGroup-T3TE2AWETDYZ

- `--nodes=AS min값:AS max값:AS 이름` 으로 설정을 해야한다.

- env value를 원하는 region으로 바꾼다

### 로그 보기

`kubectl logs -f deployment/cluster-autoscaler -n kube-system`

# 기타

`kubectl delete -f yml파일`

해당 파일의 설정을 다 삭제함

`eksctl delete cluster sample`

cluster 삭제함
